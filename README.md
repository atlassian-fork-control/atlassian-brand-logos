# Atlassian Brand Logos

This package contains a set of SVG assets for the various [Atlassian][atlassian] product and program logos.

The complete set of logo assets can be found on [Atlassian's design website][adg-logos].

## Structure

All assets are in a `dist/` folder.

```text
+-- atlassian
│   +-- atlassian-horizontal-blue.svg
│   +-- atlassian-horizontal-neutral.svg
│   \-- atlassian-horizontal-white.svg
+-- aui-legacy
│   +-- aui-footer.css
│   \-- aui-header.css
+-- favicons
│   +-- fav-atlassian.ico
│   +-- fav-atlassian.png
│   +-- fav-bamboo.ico
│   +-- fav-bamboo.png
│   +-- fav-bitbucket.ico
│   +-- fav-bitbucket.png
│   +-- fav-confluence.ico
│   +-- fav-confluence.png
│   +-- fav-crowd.ico
│   +-- fav-crowd.png
│   +-- fav-crucible.ico
│   +-- fav-crucible.png
│   +-- fav-fisheye.ico
│   +-- fav-fisheye.png
│   +-- fav-hipchat.ico
│   +-- fav-hipchat.png
│   +-- fav-jcore.ico
│   +-- fav-jcore.png
│   +-- fav-jira.ico
│   +-- fav-jira.png
│   +-- fav-jsd.ico
│   +-- fav-jsd.png
│   +-- fav-jsw.ico
│   \-- fav-jsw.png
+-- products
│   +-- bamboo-white.svg
│   +-- bitbucket-white.svg
│   +-- clover-white.svg
│   +-- confluence-white.svg
│   +-- crowd-white.svg
│   +-- crucible-white.svg
│   +-- fisheye-white.svg
│   +-- hipchat-white.svg
│   +-- jira-core-white.svg
│   +-- jira-service-desk-white.svg
│   +-- jira-software-white.svg
│   +-- jira-white.svg
│   +-- jira-with-core-white.svg
│   +-- jira-with-service-desk-white.svg
│   +-- jira-with-software-white.svg
│   +-- sourcetree-white.svg
│   +-- statuspage-white.svg
│   \-- stride-white.svg
\-- programs
    +-- atlassian-community-white.svg
    +-- atlassian-developer-white.svg
    +-- atlassian-marketplace-white.svg
    +-- atlassian-partner-program-white.svg
    +-- atlassian-support-white.svg
    \-- atlassian-university-white.svg
```

## Licensing

Atlassian's brand logos are licensed under the [Atlassian Design Guidelines license][adg-license].

[atlassian]: https://atlassian.com
[adg-logos]: https://atlassian.design/guidelines/brand/logos
[adg-license]: https://atlassian.design/guidelines/handy/license
